﻿using UnityEngine;
using System.Collections;

public class CameraPixelPerfectVFX : MonoBehaviour
{

    public float maxPixelHeight;

    void Awake()
    {
        float scale = Mathf.Ceil(Screen.height / maxPixelHeight);
        Camera.main.orthographicSize = Screen.height / 2f / scale;
    }
}