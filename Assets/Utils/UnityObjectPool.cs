﻿using UnityEngine;

using System;
using System.Collections;


namespace IntergalacticJammernauts
{

	using Object = UnityEngine.Object;


	public class UnityObjectPool< T > : ObjectPool< T > where T : Component
	{

		private readonly Transform _objectsRoot;


		public UnityObjectPool( Component o, int poolSize = 20 ) : this( o, DefaultFetchAction, DefaultReleaseAction, poolSize )
		{
		}


		public UnityObjectPool( Component o, Action<T> fetchAction, Action<T> returnAction, int poolSize = 20 ) : base( fetchAction, returnAction )
		{
			var rootObject = new GameObject( "[ObjectPool]/" + o.name );
			_objectsRoot = rootObject.transform;

			CreateFunc = () =>
			{
				var instance = ( T ) Object.Instantiate( o );
				instance.transform.SetParent( _objectsRoot, false );
				return instance;
			};

			IncreasePool( poolSize );
		}


		~UnityObjectPool()
		{
			if ( _objectsRoot != null )
			{
				Object.Destroy( _objectsRoot.gameObject );
			}
		}


		private static void DefaultFetchAction( T obj )
		{
			obj.gameObject.SetActive( true );
		}


		private static void DefaultReleaseAction( T obj )
		{
			obj.gameObject.SetActive( false );
		}

	}

}