﻿using System.Diagnostics;


namespace IntergalacticJammernauts
{

	public struct PooledObject< T >
	{

		private T _obj;

		private ObjectPool< T > _pool;


		public bool IsActive
		{
			get
			{
				return _pool != null;
			}
		}


		public T Obj
		{
			get
			{
				return _obj;
			}
		}


		public PooledObject( ObjectPool< T > pool, T obj )
		{
			Debug.Assert( pool != null );
			Debug.Assert( obj != null );

			_pool = pool;
			_obj = obj;
		}


		public void Release()
		{
			// TODO since this is a struct, there's the risk of trying to return the same object twice
			if ( _pool != null )
			{
				_pool.Release( _obj );
				_pool = null;
				_obj = default ( T );
			}
		}

	}

}
