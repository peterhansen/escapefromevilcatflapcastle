﻿using UnityEngine;

using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

using Object = UnityEngine.Object;


namespace PitecoHansen
{

	public static class UnityExtensions
	{

		/// <summary>
		/// Extension method to get a gameobject's component, adding it if necessary.
		/// </summary>
		public static C GetOrAddComponent<C>( this MonoBehaviour script ) where C : Component
		{
			return script.gameObject.GetOrAddComponent<C>();
		}


		/// <summary>
		/// Extension method to get a gameobject's component, adding it if necessary.
		/// </summary>
		public static C GetOrAddComponent<C>( this GameObject gameObject ) where C : Component
		{
			C component = gameObject.GetComponent<C>();
			if ( component == null )
			{
				component = gameObject.AddComponent<C>();
			}

			return component; 
		}


		/// <summary>
		/// Extension method to get a gameobject's component, adding it if necessary.
		/// </summary>
		public static Component GetOrAddComponent( this MonoBehaviour script, Type componentType )
		{
			return script.gameObject.GetOrAddComponent( componentType );
		}


		/// <summary>
		/// Extension method to get a gameobject's component, adding it if necessary.
		/// </summary>
		public static Component GetOrAddComponent( this GameObject gameObject, Type componentType )
		{
			var component = gameObject.GetComponent( componentType );
			if ( component == null )
			{
				component = gameObject.AddComponent( componentType );
			}

			return component;
		}


		/// <summary>
		/// Extension method to remove a gameobject's component.
		/// </summary>
		public static void RemoveComponent<C>( this MonoBehaviour script ) where C : Component
		{
			script.gameObject.RemoveComponent<C>();
		}


		/// <summary>
		/// Extension method to remove a gameobject's component.
		/// </summary>
		public static void RemoveComponent<C>( this GameObject gameObject ) where C : Component
		{
			C component = gameObject.GetComponent<C>();
			if ( component != null )
			{
				DestroyObject( component );
			}
		}


		/// <summary>
		/// Extension method to remove all components of a given type C from a gameobject's hierarchy.
		/// </summary>
		public static void RemoveComponentsInChildren<C>( this MonoBehaviour script, bool includeInactive = true ) where C : Component
		{
			script.gameObject.RemoveComponentsInChildren<C>( includeInactive );
		}


		/// <summary>
		/// Extension method to remove all components of a given type C from a gameobject's hierarchy.
		/// </summary>
		public static void RemoveComponentsInChildren<C>( this GameObject gameObject, bool includeInactive = true  ) where C : Component
		{
			var childrenComponents = gameObject.GetComponentsInChildren<C>( includeInactive );
			for ( var index = 0; index < childrenComponents.Length; ++index )
			{
				var component = childrenComponents[ index ];
				DestroyObject( component );
			}
		}


		/// <summary>
		/// Extension method to delete all children of a MonoBehaviour, whether they are active or not.
		/// </summary>
		public static void DestroyAllChildren( this MonoBehaviour script )
		{
			DestroyAllChildren( script.transform );
		}


		/// <summary>
		/// Extension method to delete all children of a GameObject, whether they are active or not.
		/// </summary>
		public static void DestroyAllChildren( this Transform transform )
		{
			for ( var i = transform.childCount - 1; i >= 0; --i )
			{
				DestroyObject( transform.GetChild( i ).gameObject );
			}
		}


		/// <summary>
		/// Extension method to enumerate all components of the type C in a GameObject's children with a given name. 
		/// This includes the GameObject, and also disabled gameobject children on its hierarchy.
		/// </summary>
		public static IEnumerable<C> GetComponentsInChildren<C>( this MonoBehaviour script, string name ) where C : Component
		{
			foreach ( C component in GetComponentsInChildren<C>( script.gameObject, name ) )
			{
				yield return component;
			}
		}


		/// <summary>
		/// Extension method to enumerate all components of the type C in a GameObject's children with a given name. 
		/// This includes the GameObject, and also disabled gameobject children on its hierarchy.
		/// </summary>
		public static IEnumerable< C > GetComponentsInChildren<C>( this GameObject obj, string name ) where C : Component
		{
			var childrenComponents = obj.GetComponentsInChildren<C>( true );
			for ( var index = 0; index < childrenComponents.Length; ++index )
			{
				var component = childrenComponents[ index ];
				if ( component.name.Equals( name ) )
				{
					yield return component;
				}
			}
		}


		/// <summary>
		/// Extension method to enumerate all components of the type C in a GameObject's children which names match a given regex.
		/// This includes the GameObject, and also disabled gameobject children on its hierarchy.
		/// </summary>
		public static IEnumerable<C> GetComponentsInChildren<C>( this MonoBehaviour script, Regex regex ) where C : Component
		{
			foreach ( C component in GetComponentsInChildren<C>( script.gameObject, regex ) )
			{
				yield return component;
			}
		}


		/// <summary>
		/// Extension method to enumerate all components of the type C in a GameObject's children which names match a given regex.
		/// This includes the GameObject, and also disabled gameobject children on its hierarchy.
		/// </summary>
		public static IEnumerable<C> GetComponentsInChildren<C>( this GameObject obj, Regex regex ) where C : Component
		{
			var childrenComponents = obj.GetComponentsInChildren<C>( true );
			for ( var index = 0; index < childrenComponents.Length; ++index )
			{
				var component = childrenComponents[ index ];
				if ( regex.IsMatch( component.name ) )
				{
					yield return component;
				}
			}
		}


		/// <summary>
		/// Returns the first component of the type C in a GameObject's children with a given name. 
		/// The search includes the GameObject, and also disabled gameobject children on its hierarchy.
		/// If no component is found, null is returned.
		/// </summary>
		public static C GetComponentInChildren<C>( this MonoBehaviour script, string name ) where C : Component
		{
			return GetComponentInChildren<C>( script.gameObject, name );
		}


		/// <summary>
		/// Returns the first component of the type C in a GameObject's children with a given name. 
		/// The search includes the GameObject, and also disabled gameobject children on its hierarchy.
		/// If no component is found, null is returned.
		/// </summary>
		public static C GetComponentInChildren<C>( this GameObject obj, string name ) where C : Component
		{
			var childrenComponents = obj.GetComponentsInChildren<C>( true );
			for ( var index = 0; index < childrenComponents.Length; ++index )
			{
				C component = childrenComponents[ index ];
				if ( component.name.Equals( name ) )
				{
					return component;
				}
			}

			return null;
		}


		public static void ResetLocal( this Transform transform )
		{
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			transform.localScale = Vector3.one;
		}


		public static Transform FindRecursiveDepthFirst( this Transform transform, string name, bool ignoreCase = false )
		{
			var stringComparisonMode = ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;
			var childCount = transform.childCount;
			for ( var i = 0; i < childCount; ++i )
			{
				var child = transform.GetChild( i );
				if ( child.name.Equals( name, stringComparisonMode ) )
				{
					return child;
				}

				child = FindRecursiveDepthFirst( child, name, ignoreCase );
				if ( child != null )
				{
					return child;
				}
			}

			return null;
		}


		/// <summary>
		/// Destroy an Unity object, handling the different method calls whether it is being called during Editor mode or not.
		/// </summary>
		private static void DestroyObject( Object obj )
		{
#if UNITY_EDITOR
				if ( Application.isPlaying )
				{
					Object.Destroy( obj );
				}
				else
				{
					Object.DestroyImmediate( obj );
				}
#else
				Object.Destroy( obj );
#endif
		}

	}

}
