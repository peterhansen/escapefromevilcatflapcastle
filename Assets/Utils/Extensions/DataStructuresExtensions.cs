﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataStructuresExtensions
{

	public static T Last< T >( this T[] array )
	{
		var length = array.Length;
		if ( length > 0 )
		{
			return array[ length - 1 ];
		}

		return default( T );
	}


	public static T Last< T >( this List< T > list )
	{
		var count = list.Count;
		if ( count > 0 )
		{
			return list[ count - 1 ];
		}

		return default( T );
	}

}
