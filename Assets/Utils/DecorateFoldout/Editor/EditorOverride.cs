using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Homebrew
{
	[CustomEditor(typeof(Object), true, isFallback = true)]
	[CanEditMultipleObjects]
	public class EditorOverride : Editor
	{
		private readonly Dictionary<string, Cache> m_cache = new Dictionary<string, Cache>();
		private readonly List<SerializedProperty> m_props = new List<SerializedProperty>();
		private SerializedProperty m_propScript;
		private Type m_type;
		private int m_length;
		private List<FieldInfo> m_objectFields;
		private bool m_initialized;
		private Colors colors;
		private FoldoutAttribute m_prevFold;
		private GUIStyle m_style;


		private void Awake()
		{
			var uiTex_in = Resources.Load<Texture2D>("IN foldout focus-6510");
			var uiTex_in_on = Resources.Load<Texture2D>("IN foldout focus on-5718");


			var c_on = Color.white;

			m_style = new GUIStyle( EditorStyles.foldout )
			{
				overflow = new RectOffset( -10, 0, 3, 0 ),
				padding = new RectOffset( 25, 0, -3, 0 ),
				active =
				{
					textColor = c_on,
					background = uiTex_in
				},
				onActive =
				{
					textColor = c_on,
					background = uiTex_in_on
				},
				focused =
				{
					textColor = c_on,
					background = uiTex_in
				},
				onFocused =
				{
					textColor = c_on,
					background = uiTex_in_on
				}
			};
		}


		private void OnEnable()
		{
			bool pro = EditorGUIUtility.isProSkin;
			if (!pro)
			{
				colors = new Colors();
				colors.col0 = new Color(0.2f, 0.2f, 0.2f, 1f);
				colors.col1 = new Color(1, 1, 1, 0.55f);
				colors.col2 = new Color(0.7f, 0.7f, 0.7f, 1f);
			}
			else
			{
				colors = new Colors();
				colors.col0 = new Color(0.2f, 0.2f, 0.2f, 1f);
				colors.col1 = new Color(1, 1, 1, 0.1f);
				colors.col2 = new Color(0.25f, 0.25f, 0.25f, 1f);
			}
			
			var t = target.GetType();
			var typeTree = t.GetTypeTree();
			m_objectFields = target.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
				.OrderByDescending(x => typeTree.IndexOf(x.DeclaringType)).ToList();

			m_length = m_objectFields.Count;
			
			Repaint();
			m_initialized = false;
		}


		private void OnDisable()
		{
			foreach (var cach in m_cache)
			{
				cach.Value.Dispose();
			}
		}


		public override void OnInspectorGUI()
		{
			serializedObject.Update();
			Initialize();

			if ( m_props.Count == 0 )
			{
				DrawDefaultInspector();
				return;
			}

			m_initialized = true;

			using ( new EditorGUI.DisabledScope( "m_Script" == m_props[ 0 ].propertyPath ) )
			{
				EditorGUILayout.PropertyField( m_props[ 0 ], true );
			}

			EditorGUILayout.Space();

			foreach ( var pair in m_cache )
			{
				var rect = EditorGUILayout.BeginVertical();

				EditorGUILayout.Space();

				EditorGUI.DrawRect( new Rect( rect.x - 1, rect.y - 1, rect.width + 1, rect.height + 1 ),
					colors.col0 );

				EditorGUI.DrawRect( new Rect( rect.x - 1, rect.y - 1, rect.width + 1, rect.height + 1 ), colors.col1 );


				pair.Value.Expanded = EditorGUILayout.Foldout( pair.Value.Expanded, pair.Value.Atr.Name, true, m_style ?? EditorStyles.foldout );

				EditorGUILayout.EndVertical();

				rect = EditorGUILayout.BeginVertical();

				EditorGUI.DrawRect( new Rect( rect.x - 1, rect.y - 1, rect.width + 1, rect.height + 1 ), colors.col2 );

				if ( pair.Value.Expanded )
				{
					EditorGUILayout.Space();
					{
						for ( int i = 0; i < pair.Value.Props.Count; i++ )
						{
							EditorGUI.indentLevel = 1;

							EditorGUILayout.PropertyField( pair.Value.Props[ i ], new GUIContent( pair.Value.Props[ i ].name.FirstLetterToUpperCase() ), true );
							if ( i == pair.Value.Props.Count - 1 )
							{
								EditorGUILayout.Space();
							}
						}
					}
				}

				EditorGUI.indentLevel = 0;
				EditorGUILayout.EndVertical();
				EditorGUILayout.Space();
			}


			for ( var i = 1; i < m_props.Count; i++ )
			{
				EditorGUILayout.PropertyField( m_props[ i ], true );
			}


			serializedObject.ApplyModifiedProperties();
			EditorGUILayout.Space();
		}

		private void Initialize()
		{
			if ( m_initialized )
			{
				return;
			}

			for ( var i = 0; i < m_length; i++ )
			{
				var fold = Attribute.GetCustomAttribute( m_objectFields[ i ], typeof( FoldoutAttribute ) ) as FoldoutAttribute;

				Cache c;
				if ( fold == null )
				{
					if ( m_prevFold != null && m_prevFold.FoldEverything )
					{
						if ( !m_cache.TryGetValue( m_prevFold.Name, out c ) )
						{
							m_cache.Add( m_prevFold.Name, new Cache { Atr = m_prevFold, Types = new HashSet<string> { m_objectFields[ i ].Name } } );
						}
						else
						{
							c.Types.Add( m_objectFields[ i ].Name );
						}
					}

					continue;
				}

				m_prevFold = fold;
				if ( !m_cache.TryGetValue( fold.Name, out c ) )
				{
					m_cache.Add( fold.Name, new Cache { Atr = fold, Types = new HashSet<string> { m_objectFields[ i ].Name } } );
				}
				else
				{
					c.Types.Add( m_objectFields[ i ].Name );
				}
			}


			var property = serializedObject.GetIterator();
			var next = property.NextVisible( true );
			if ( next )
			{
				do
				{
					HandleProp( property );
				}
				while ( property.NextVisible( false ) );
			}
		}


		public void HandleProp(SerializedProperty prop)
		{
			bool shouldBeFolded = false;

			foreach (var pair in m_cache)
			{
				if (pair.Value.Types.Contains(prop.name))
				{
					shouldBeFolded = true;
					pair.Value.Props.Add(prop.Copy());

					break;
				}
			}

			if ( !shouldBeFolded )
			{
				m_props.Add(prop.Copy());
			}
		}


		private struct Colors
		{
			public Color col0;
			public Color col1;
			public Color col2;
		}


		private class Cache
		{
			public HashSet<string> Types = new HashSet<string>();
			public readonly List<SerializedProperty> Props = new List<SerializedProperty>();
			public FoldoutAttribute Atr;
			public bool Expanded;


			public void Dispose()
			{
				Props.Clear();
				Types.Clear();
				Atr = null;
			}

		}

	}


	public static class FrameworkExtensions
	{
		public static string FirstLetterToUpperCase(this string s)
		{
			if ( string.IsNullOrEmpty( s ) )
			{
				return string.Empty;
			}

			char[] a = s.ToCharArray();
			a[0] = char.ToUpper(a[0]);
			return new string(a);
		}


		public static IList<Type> GetTypeTree(this Type t)
		{
			var types = new List<Type>();
			while (t.BaseType != null)
			{
				types.Add(t);
				t = t.BaseType;
			}

			return types;
		}

	}

}
