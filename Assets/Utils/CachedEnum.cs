﻿using System;


public static class CachedEnum< T > where T : struct, IComparable, IFormattable, IConvertible
{

	public static T[] Values { get; private set; }

	public static string[] Names { get; private set; }


	static CachedEnum()
	{
		Values = ( T[] ) Enum.GetValues( typeof( T ) );
		Names = Enum.GetNames( typeof( T ) );
	}

}
