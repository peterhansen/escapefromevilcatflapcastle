﻿using System;
using System.Collections.Generic;

using IntergalacticJammernauts;

using UnityEditor;

using UnityEngine;

using Object = UnityEngine.Object;


namespace PitecoHansen
{

	[ CustomPropertyDrawer( typeof( AutoAddAttribute ), true ) ]
	public class AutoAddAttributeDrawer : PropertyDrawer
	{

		private bool m_hasShownErrorMessage;


		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			if ( !property.objectReferenceValue )
			{
				var monoBehaviour = property.serializedObject.targetObject as MonoBehaviour;
				if ( !monoBehaviour )
				{
					if ( !m_hasShownErrorMessage )
					{
						m_hasShownErrorMessage = true;
						Debug.LogErrorFormat( property.serializedObject.targetObject, "[AutoAdd] attribute can only be added to MonoBehaviour scripts." );
					}
					return;
				}

				var autoAddAttribute = attribute as AutoAddAttribute;
				if ( autoAddAttribute.SearchComponentInChildren )
				{
					property.objectReferenceValue = monoBehaviour.GetComponentInChildren( fieldInfo.FieldType );
					if ( property.objectReferenceValue == null )
					{
						property.objectReferenceValue = monoBehaviour.GetOrAddComponent( fieldInfo.FieldType );
					}
				}
				else
				{
					property.objectReferenceValue = monoBehaviour.GetOrAddComponent( fieldInfo.FieldType );
				}
			}

			EditorGUI.PropertyField( position, property, label, true );
			m_hasShownErrorMessage = false;
		}

	}

}
