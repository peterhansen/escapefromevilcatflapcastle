﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using UnityEditor;

using UnityEngine;


namespace PitecoHansen
{

	[ CustomPropertyDrawer( typeof( PopulateDropdownAttribute ), true ) ]
	public class PopulateDropdownAttributeDrawer : PropertyDrawer
	{

		private const BindingFlags DrawerBindingFlags = BindingFlags.IgnoreCase | BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

		private GUIContent[] m_options = new GUIContent[ 0 ];

		private readonly List< object > m_values = new List< object >();

		private PopulateDropdownAttribute m_attribute;

		private bool m_initialized;

		private string m_tooltip;

		private int m_index;

		private int m_offset;


		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			Initialize( property );

			if ( m_options.Length > 0 )
			{
				EditorGUI.showMixedValue = property.hasMultipleDifferentValues;
				position.height = Mathf.Min( position.height, EditorGUIUtility.singleLineHeight );
				var index = EditorGUI.Popup( position, new GUIContent( property.displayName, m_tooltip ), m_index, m_options );
				if ( index != m_index )
				{
					m_index = index;

					var offsetIndex = index - m_offset;
					if ( offsetIndex >= 0 )
					{
						property.SetValue( m_values[ offsetIndex ] );
					}
				}

				if ( index == 0 && m_attribute.AllowManualEntry )
				{
					position.x += EditorGUIUtility.labelWidth;
					position.width -= EditorGUIUtility.labelWidth;
					position.y += EditorGUIUtility.singleLineHeight;
					EditorGUI.PropertyField( position, property, new GUIContent(), true );
				}

				EditorGUI.showMixedValue = false;
			}
			else
			{
				EditorGUI.PropertyField( position, property, label, true );
			}
		}


		public override float GetPropertyHeight( SerializedProperty property, GUIContent label )
		{
			var isCustom = m_index == 0 && m_attribute != null && m_attribute.AllowManualEntry;
			var extraHeight = isCustom ? EditorGUIUtility.singleLineHeight : 0;
			return EditorGUI.GetPropertyHeight( property, label, true ) + extraHeight;
		}


		private void Initialize( SerializedProperty property )
		{
			if ( !m_initialized )
			{
				var targetObject = property.serializedObject.targetObject;
				m_attribute = attribute as PopulateDropdownAttribute;
				m_index = 0;
				m_offset = 0;
				m_values.Clear();
				m_initialized = true;

				m_tooltip = this.GetTooltip( property );

				var objectType = targetObject.GetType();
				string[] options = null;
				var methodInfo = objectType.GetMethod( m_attribute.OptionsListMethodName, DrawerBindingFlags );
				if ( methodInfo != null )
				{
					var enumerableReturnValue = ( IEnumerable ) methodInfo.Invoke( targetObject, new object[ 0 ] );
					foreach ( var obj in enumerableReturnValue )
					{
						m_values.Add( obj );
					}
					m_offset = m_attribute.AllowManualEntry ? 1 : 0;
					options = new string[ m_values.Count + m_offset ];

					if ( m_attribute.AllowManualEntry )
					{
						options[ 0 ] = "<custom>";
					}

					for ( var i = 0; i < m_values.Count; ++i )
					{
						options[ i + m_offset ] = m_values[ i ].ToString();
					}
				}

				if ( options != null )
				{
					var currentValueAsString = property.GetPropertyValue().ToString();

					m_options = new GUIContent[ options.Length ];
					for ( var i = 0; i < options.Length; ++i )
					{
						m_options[ i ] = new GUIContent( options[ i ] );

						if ( options[ i ].Equals( currentValueAsString, StringComparison.InvariantCultureIgnoreCase) )
						{
							m_index = i;
						}
					}
				}
				else
				{
					Debug.LogErrorFormat( targetObject, "Could not find a method called '{0}' on the target object to auto-populate options list.", m_attribute.OptionsListMethodName );
				}
			}
		}

	}

}
