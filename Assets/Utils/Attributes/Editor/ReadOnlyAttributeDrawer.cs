﻿using System;
using System.Collections.Generic;

using IntergalacticJammernauts;

using UnityEditor;

using UnityEngine;

using Object = UnityEngine.Object;


namespace PitecoHansen
{

	[ CustomPropertyDrawer( typeof( ReadOnlyAttribute ), true ) ]
	public class ReadOnlyAttributeDrawer : PropertyDrawer
	{

		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			var wasGUIEnabled = GUI.enabled;
			var readOnlyAttribute = ( ReadOnlyAttribute ) attribute;
			if ( EditorApplication.isPlayingOrWillChangePlaymode )
			{
				if ( readOnlyAttribute.Mode == ReadOnlyMode.HideWhenPlaying )
				{
					return;
				}

				GUI.enabled = false;
			}
			else if ( readOnlyAttribute.Mode == ReadOnlyMode.AlwaysDisabled )
			{
				GUI.enabled = false;
			}

			EditorGUI.PropertyField( position, property, label, true );
			GUI.enabled = wasGUIEnabled;
		}


		public override float GetPropertyHeight( SerializedProperty property, GUIContent label )
		{
			var readOnlyAttribute = ( ReadOnlyAttribute ) attribute;

			if ( EditorApplication.isPlayingOrWillChangePlaymode && readOnlyAttribute.Mode == ReadOnlyMode.HideWhenPlaying )
			{
				return 0.0f;
			}

			return EditorGUI.GetPropertyHeight( property, label, true );
		}

	}

}
