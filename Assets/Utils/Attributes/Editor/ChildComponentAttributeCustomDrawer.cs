﻿using System;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

using Object = UnityEngine.Object;


namespace PitecoHansen
{

	[ CustomPropertyDrawer( typeof( ChildComponentAttribute ), true ) ]
	public class ChildComponentCustomDrawer : PropertyDrawer
	{

		private bool m_initialized;

		private Object[] m_objects;

		private GUIContent[] m_paths;

		private int m_previousIndex;

		private string m_tooltip;


		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			if ( !m_initialized )
			{
				RefreshOptions( property );
			}

			EditorGUI.showMixedValue = property.hasMultipleDifferentValues;
			var index = EditorGUI.Popup( position, new GUIContent( property.displayName, m_tooltip ), m_previousIndex, m_paths );
			if (index != m_previousIndex)
			{
				m_previousIndex = index;
				property.objectReferenceValue = m_objects[index];
			}

			EditorGUI.showMixedValue = false;

			property.PingTargetOnClick( position );
		}


		private void RefreshOptions( SerializedProperty property )
		{
			var monoBehaviour = property.serializedObject.targetObject as MonoBehaviour;
			if (!monoBehaviour)
			{
				m_initialized = true;
				Debug.LogErrorFormat(property.serializedObject.targetObject, "[ChildComponent] attribute can be used only on MonoBehaviour scripts.");
				return;
			}

			var allChildrenComponents = monoBehaviour.GetComponentsInChildren(fieldInfo.FieldType);

			var count = allChildrenComponents.Length + 1;
			m_objects = new Object[ count ];
			Array.Copy( allChildrenComponents, 0, m_objects, 1, count - 1 );

			var propertyTargetObject = property.objectReferenceValue;

			m_paths = new GUIContent[ count ];
			m_paths[ 0 ] = new GUIContent( "<none>" );
			m_previousIndex = 0;
			for ( int i = 1; i < count; i++ )
			{
				var obj = m_objects[ i ];
				m_paths[ i ] = new GUIContent( obj.name );

				if ( obj == propertyTargetObject )
				{
					m_previousIndex = i;
				}
			}

			m_tooltip = this.GetTooltip( property );
		}


		/// <summary>
		/// Handles clicks on the PropertyDrawer's label, "pinging" the target object on the project view.
		/// </summary>
		private void HandleEvents( Rect rect, Object target )
		{
			if ( target == null )
			{
				return;
			}

			var evt = Event.current;
			if ( evt.type == EventType.MouseDown )
			{
				rect.width = EditorGUIUtility.labelWidth;
				var mousePos = evt.mousePosition;
				if ( rect.Contains( mousePos ) )
				{
					EditorGUIUtility.PingObject( ( ( Component ) target ).gameObject );
					evt.Use();
				}
			}
		}

	}

}
