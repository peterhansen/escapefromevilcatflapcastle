﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;


namespace PitecoHansen
{

	[ CustomPropertyDrawer( typeof ( PrefabPickerAttribute ), true ) ]
	public class SmartObjectPickerCustomDrawer : PropertyDrawer
	{

		private GUIContent[] _paths;

		private Object[] _objects;

		private int _previousIndex;

		private string _tooltip;

		private bool _initialized;


		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			if ( !_initialized )
			{
				var type = fieldInfo.FieldType.IsArray ? fieldInfo.FieldType.GetElementType() : fieldInfo.FieldType;
				var allPrefabs = AssetDatabase.FindAssets( "t:GameObject" );
				var list = new List< GUIContent >( 16 );
				var tempObjectList = new List< Object >();
				var startIndex = "Assets".Length + 1;

				list.Add( new GUIContent( "<none>" ) );
				tempObjectList.Add( null );

				var currentAssetPath = AssetDatabase.GetAssetPath( property.objectReferenceValue );

				for ( var i = 0; i < allPrefabs.Length; i++ )
				{
					var prefabGuid = allPrefabs[ i ];
					var path = AssetDatabase.GUIDToAssetPath( prefabGuid );
					var asset = AssetDatabase.LoadMainAssetAtPath( path ) as GameObject;
					if ( asset != null && asset.GetComponent( type ) != null )
					{
						if ( string.Equals( currentAssetPath, path ) )
						{
							_previousIndex = tempObjectList.Count;
						}

						tempObjectList.Add( asset );
						path = path.Substring( startIndex ).Replace( ".prefab", string.Empty ).Replace( '\\', '/' );
						list.Add( new GUIContent( path, path ) );
					}
				}

				_paths = list.ToArray();
				_objects = tempObjectList.ToArray();

				_tooltip = this.GetTooltip( property );

				_initialized = true;
			}

			EditorGUI.showMixedValue = property.hasMultipleDifferentValues;
			var index = EditorGUI.Popup( position, new GUIContent( property.displayName, _tooltip ), _previousIndex, _paths );
			if ( index != _previousIndex )
			{
				_previousIndex = index;
				property.objectReferenceValue = _objects[ index ];
			}
			EditorGUI.showMixedValue = false;

			property.PingTargetOnClick( position );
		}

	}

}
