﻿using UnityEngine;

using System;


namespace PitecoHansen
{

	/// <summary>
	/// <para/>Adding this attribute to a class/struct member ensures that the member will be automatically assigned with a component found on the 
	/// GameObject (or, alternatively, on one of its children).
	/// <para/>If a component of the same type doesn't exist, it will be automatically added.
	/// <para>This has a similar effect to using the [RequireComponent] attribute, with the extra advantage of automatically assigning the reference to a member,
	/// caching it without the need of calling GetComponent() during runtime.</para>
	/// </summary>
	[AttributeUsage( AttributeTargets.Field ) ]
	public class AutoAddAttribute : PropertyAttribute
	{

		public readonly bool SearchComponentInChildren;


		public AutoAddAttribute() : this( false )
		{
		}


		public AutoAddAttribute( bool searchComponentInChildren )
		{
			SearchComponentInChildren = searchComponentInChildren;
		}

	}

}
