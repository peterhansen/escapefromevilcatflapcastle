﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Add this attribute to a class/struct member to make it editable only when NOT playing the game on Editor.
/// By default, the field is just greyed out (disabled), but it can alternatively be hidden instead.
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class ReadOnlyAttribute : PropertyAttribute
{

	public readonly ReadOnlyMode Mode;


	public ReadOnlyAttribute() : this( ReadOnlyMode.DisableWhenPlaying )
	{
	}


	public ReadOnlyAttribute( ReadOnlyMode readonlyMode )
	{
		Mode = readonlyMode;
	}

}


public enum ReadOnlyMode
{
	/// <summary>
	/// Field is disabled when playing on Editor.
	/// </summary>
	DisableWhenPlaying,
	/// <summary>
	/// Field isn't visible when playing on Editor.
	/// </summary>
	HideWhenPlaying,
	/// <summary>
	/// Field is always disabled. Useful for auto-generated fields.
	/// </summary>
	AlwaysDisabled,
}
