﻿using UnityEngine;

using System;


namespace PitecoHansen
{

	/// <summary>
	/// Add this attribute to any serialized member of your scripts to replace Unity's default object picker with a list that ensures that all options:
	/// - are prefabs and NOT objects on a scene
	/// - have the a script of the member type on its root
	/// </summary>
	[ AttributeUsage( AttributeTargets.Field ) ]
	public class PrefabPickerAttribute : PropertyAttribute
	{
	}

}
