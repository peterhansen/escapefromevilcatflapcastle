﻿using UnityEngine;

using System;


namespace PitecoHansen
{

	/// <summary>
	/// Add this attribute to any serialized member of your scripts to replace Unity's default object picker with a list that ensures that all options
	/// are components found on the GameObject or on one of its children.
	/// </summary>
	[ AttributeUsage( AttributeTargets.Field ) ]
	public class ChildComponentAttribute : PropertyAttribute
	{
	}

}
