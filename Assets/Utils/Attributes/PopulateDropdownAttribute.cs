﻿using UnityEngine;

using System;


namespace PitecoHansen
{

	/// <summary>
	/// <para/> Add this attribute to a class/struct member to expose it as a dropdown list on Unity inspector.
	/// <para/> The constructor parameter is the name of a property or method that returns the list of options. This property or method:
	/// <para/> - needs to return an IEnumerable or IEnumerable&lt;object&gt; or IEnumerable&lt;T&gt; (where T is the same type or a subtype of the declared member)
	/// <para/> - can be static or instanced
	/// <para/> - can be public, protected or private
	/// <para/> - will be called only on Unity Editor
	/// </summary>
	[AttributeUsage( AttributeTargets.Field ) ]
	public class PopulateDropdownAttribute : PropertyAttribute
	{

		public readonly string OptionsListMethodName;

		public readonly bool AllowManualEntry;


		public PopulateDropdownAttribute( string optionsListMethodName ) : this( optionsListMethodName, false )
		{
		}


		public PopulateDropdownAttribute( string optionsListMethodName, bool allowManualEntry )
		{
			OptionsListMethodName = optionsListMethodName;
			AllowManualEntry = allowManualEntry;
		}

	}

}
