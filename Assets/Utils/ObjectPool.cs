﻿
using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;


namespace IntergalacticJammernauts
{

	public class ObjectPool< T >
	{

		private readonly List< T > _pool = new List< T >();

		private readonly Stack< T > _availableStack = new Stack<T>();

		private readonly Action< T > _fetchAction;

		private readonly Action< T > _returnAction;


		protected Func< T > CreateFunc
		{
			get;
			set;
		}


		public ObjectPool( Func< T > createFunc, Action< T > fetchAction, Action< T > returnAction, int poolSize = 20 )
		{
			CreateFunc = createFunc;
			_fetchAction = fetchAction;
			_returnAction = returnAction;

			IncreasePool( poolSize );
		}


		protected ObjectPool( Action<T> fetchAction, Action<T> returnAction )
		{
			_fetchAction = fetchAction;
			_returnAction = returnAction;
		}


		public PooledObject< T > Fetch()
		{
			if ( _availableStack.Count == 0 )
			{
				IncreasePool( _pool.Count >> 1 );
			}

			var obj = _availableStack.Pop();
			_fetchAction( obj );

			return new PooledObject< T >( this, obj );
		}


		public void Release( T obj )
		{
			Debug.AssertFormat( !_availableStack.Contains( obj ), "Attempt to release pooled object already in pool." );

			_returnAction( obj );
			_availableStack.Push( obj );
		}


		protected void IncreasePool( int amount )
		{
			for ( var i = 0; i < amount; ++i )
			{
				var newInstance = CreateFunc();
				_pool.Add( newInstance );
				_availableStack.Push( newInstance );

				_returnAction( newInstance );
			}
		}

	}

}