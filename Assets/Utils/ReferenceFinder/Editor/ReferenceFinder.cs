﻿using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using Object = UnityEngine.Object;


namespace PitecoHansen
{

	public class ReferenceFinder : EditorWindow
	{

		private static readonly string[] Extensions = {
			".prefab", ".unity"
		};

		private readonly List< ReferenceInfo > _references = new List< ReferenceInfo >();


		public List< ReferenceInfo > References
		{
			get
			{
				return _references;
			}
		}


		private Vector2 ScrollPosition { get; set; }

		private readonly List< Object > _nonReferencedAssets = new List< Object >();

		// TODO add method to show only objects without referencing files
		// TODO add option to search all assets of selected types
		// TODO fix delete asset Editor crash

		[ MenuItem( "Assets/Find all references in project" ) ]
		private static void FindAssetProjectReferences()
		{
			var t = EditorApplication.timeSinceStartup;

			var referenceFinder = GetWindow<ReferenceFinder>( "Reference Finder" );

			referenceFinder._references.Clear();

			foreach ( var selectedObject in Selection.GetFiltered( typeof( Object ), SelectionMode.DeepAssets ) )
			{
				// filter folders out
				var assetPath = AssetDatabase.GetAssetPath( selectedObject );
				if ( !Directory.Exists( assetPath ) )
				{
					var referenceInfo = new ReferenceInfo( selectedObject );
					referenceFinder.References.Add( referenceInfo );
				}
			}
			
			var allFiles = Directory.GetFiles( Application.dataPath, "*.*", SearchOption.AllDirectories )
				.Where( file => Extensions.Any( extension => file.EndsWith( extension, StringComparison.InvariantCultureIgnoreCase ) ) )
				.ToList();

			for ( var i = 0; i < allFiles.Count; ++i )
			{
				var fileProcessor = new FileProcessor( referenceFinder.References, allFiles[ i ] );
				fileProcessor.Process();
			}

			t = EditorApplication.timeSinceStartup - t;
			foreach ( var referenceInfo in referenceFinder.References )
			{
				Debug.LogFormat( "{0}: {1} reference(s) found in {2} files; time taken: {3}s", referenceInfo.UnityObject.name, referenceInfo.Count, allFiles.Count, t.ToString( "F2" ) );
			}

			referenceFinder.Show();
		}


		void OnGUI()
		{
			_nonReferencedAssets.Clear();

			ScrollPosition = EditorGUILayout.BeginScrollView( ScrollPosition );
			for ( var i = 0; i < _references.Count; )
			{
				var referenceInfo = _references[ i ];
				
				if ( referenceInfo.ReferencingFiles.Count > 0 )
				{
					DeleteButtonGUI( () =>
					{
						referenceInfo.Visible = EditorGUILayout.Foldout( referenceInfo.Visible, referenceInfo.GUIContent );
					}, ref i );
					
					if ( referenceInfo.Visible )
					{
						foreach ( var referencingFile in referenceInfo.ReferencingFiles )
						{
							GUILayout.BeginHorizontal();
							GUILayout.Space( 20 );
							if ( GUILayout.Button( referencingFile.GUIContent, EditorStyles.label, GUILayout.Height( 16 ) ) )
							{
								var obj = AssetDatabase.LoadMainAssetAtPath( referencingFile.AssetPath );
								EditorGUIUtility.PingObject( obj );
							}
							GUILayout.EndHorizontal();
						}
					}
				}
				else
				{
					_nonReferencedAssets.Add( referenceInfo.UnityObject );

					DeleteButtonGUI( () =>
					{
						GUILayout.Label( referenceInfo.GUIContent, GUILayout.Height( 16 ) );
					}, ref i );
				}
			}
			EditorGUILayout.EndScrollView();

			if ( _nonReferencedAssets.Count > 0 )
			{
				if ( GUILayout.Button( string.Format( "Delete {0} non-referenced asset{1}", _nonReferencedAssets.Count, _nonReferencedAssets.Count > 1 ? "s" : "" ) ) )
				{
					Undo.RegisterCompleteObjectUndo( _nonReferencedAssets.ToArray(), "delete non-referenced assets" );
					foreach ( var nonReferencedAsset in _nonReferencedAssets )
					{
						DestroyImmediate( nonReferencedAsset, true );
					}
					AssetDatabase.Refresh();
				}
			}
		}


		private void DeleteButtonGUI( Action labelAction, ref int index )
		{
			GUILayout.BeginHorizontal();

			labelAction();
			GUI.color = new Color( 0.5f, 1f, 0.5f );
			if ( GUILayout.Button( "Show" ) )
			{
				EditorGUIUtility.PingObject( _references[ index ].UnityObject );
				++index;
			}

			GUI.color = new Color( 1f, 0.5f, 0.5f );
			if ( GUILayout.Button( "Delete" ) )
			{
				Undo.DestroyObjectImmediate( _references[ index ].UnityObject );
				AssetDatabase.Refresh();
				_references.RemoveAt( index );
			}
			else
			{
				++index;
			}

			GUI.color = Color.white;
			GUILayout.EndHorizontal();
		}

	}

}