using System.Collections.Generic;

using UnityEditor;

using UnityEngine;


namespace PitecoHansen
{

	public class ReferenceInfo
	{

		private readonly List<ReferencingFile> _referencingFiles = new List<ReferencingFile>( 128 );

		public Object UnityObject { get; private set; }

		public string Path { get; private set; }

		public string Guid { get; private set; }

		public bool Visible { get; set; }

		public GUIContent GUIContent { get; private set; }


		public string Name
		{
			get
			{
				return UnityObject.name;
			}
		}


		public int Count
		{
			get
			{
				return ReferencingFiles.Count;
			}
		}


		public List<ReferencingFile> ReferencingFiles
		{
			get
			{
				return _referencingFiles;
			}
		}


		public ReferenceInfo( Object unityObject )
		{
			UnityObject = unityObject;
			Path = AssetDatabase.GetAssetPath( unityObject );
			Guid = AssetDatabase.AssetPathToGUID( Path );

			GUIContent = new GUIContent( string.Format( "{0} ({1})", Name, Count ), AssetDatabase.GetCachedIcon( Path ) );
		}


		public void Add( string filePath )
		{
			var referencingFile = new ReferencingFile( filePath );
			_referencingFiles.Add( referencingFile );
			GUIContent.text = string.Format( "{0} ({1})", Name, Count );
		}

	}

}