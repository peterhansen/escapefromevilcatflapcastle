﻿using UnityEditor;

using UnityEngine;


namespace PitecoHansen
{

	public class ReferencingFile
	{

		public string Path { get; private set; }

		public string AssetPath { get; private set; }

		public GUIContent GUIContent { get; private set; }


		public ReferencingFile( string path )
		{
			Path = path;
			AssetPath = path.Substring( Application.dataPath.Length - 6 );

			var relativePath = path.Substring( Application.dataPath.Length + 1 );
			GUIContent = new GUIContent( relativePath, AssetDatabase.GetCachedIcon( AssetPath ) );
		}
	}

}