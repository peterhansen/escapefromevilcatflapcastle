using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using PitecoHansen;


namespace PitecoHansen
{

	public class FileProcessor
	{

		private readonly Dictionary<string, ReferenceInfo> _infoByGuid;

		private readonly string _filePath;

		private readonly StringBuilder _stringBuilder = new StringBuilder( 128 );

		private Regex _regex;


		public FileProcessor( List<ReferenceInfo> references, string filePath )
		{
			_filePath = filePath.Replace( '\\', '/' );

			_infoByGuid = new Dictionary<string, ReferenceInfo>( references.Count );
			foreach ( var reference in references )
			{
				_infoByGuid[ reference.Guid ] = reference;
			}
		}


		public void Process()
		{
			RefreshRegex();

			using ( var fileStream = File.OpenRead( _filePath ) )
			{
				using ( var streamReader = new StreamReader( fileStream, Encoding.UTF8 ) )
				{
					string line;
					while ( ( line = streamReader.ReadLine() ) != null )
					{
						var match = _regex.Match( line );
						if ( match.Success )
						{
							var guid = match.Groups[ 1 ].Value;
							_infoByGuid[ guid ].Add( _filePath );
							_infoByGuid.Remove( guid );

							if ( !RefreshRegex() )
							{
								break;
							}
						}
					}
				}
			}
		}


		private bool RefreshRegex()
		{
			var total = _infoByGuid.Count;
			if ( total == 0 )
			{
				return false;
			}

			var count = 0;
			_stringBuilder.Length = 0;
			_stringBuilder.Append( "guid: (" );
			foreach ( var guid in _infoByGuid.Keys )
			{
				_stringBuilder.Append( guid );
				if ( count < total - 1 )
				{
					_stringBuilder.Append( "|" );
					++count;
				}
			}
			_stringBuilder.Append( ")" );

			var pattern = _stringBuilder.ToString();
			_regex = new Regex( pattern );

			return true;
		}

	}

}