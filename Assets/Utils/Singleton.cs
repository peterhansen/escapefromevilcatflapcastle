using UnityEngine;

using System;
using System.Collections;


namespace PitecoHansen
{

	public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{

		private static T _instance;

		private static bool _executing = true;

		[ SerializeField ]
		private bool _persistsOnSceneChanged;


		private void Awake()
		{
			if ( CheckInstance() )
			{
				OnAwake();
			}
		}


		private void OnEnable()
		{
			CheckInstance();
		}


		public static T Instance
		{
			get
			{
				if ( _executing )
				{
					if ( ( _instance == null ) || ( _instance.gameObject == null ) )
					{
						_instance = FindObjectOfType<T>();

						// if there is no object of this type on the scene, a GameObject is created to contain it
						if ( _instance == null )
						{
							var obj = new GameObject( string.Format( "[{0} (Singleton)]", typeof ( T ) ) );
							_instance = obj.AddComponent< T >();
						}
						else
						{
							DisableAllOtherInstances();
						}
					}
				}

				return _instance;
			}
		}


		/// <summary>
		/// Returns the instance of this class, without checking for null.
		/// </summary>
		public static T PeekInstance
		{
			get
			{
				return _instance;
			}
		}


		public static bool HasInstance
		{
			get
			{
				return _instance != null;
			}
		}


		private static void DisableAllOtherInstances()
		{
			if ( _instance != null )
			{
				foreach ( T instance in FindObjectsOfType<T>() )
				{
					if ( instance != _instance )
					{
						Debug.LogWarningFormat( instance.gameObject, "There is already an instance for Singleton<{0}> on {2}; disabling the new one (GameObject name={1})", typeof( T ), instance.name, _instance.name );
						instance.gameObject.SetActive( false );
					}
				}
			}
		}


		/// <summary>
		/// Indicates whether the singleton will be kept alive on scene changes (default is "false"). This property should be
		/// overriden by implementing classes if this is not the desired behaviour.
		/// </summary>
		protected virtual bool PersistsOnSceneChange
		{
			get
			{
				return _persistsOnSceneChanged;
			}
			set
			{
				_persistsOnSceneChanged = value;
			}
		}


		protected virtual void OnDestroy()
		{
			// it's important to check if this is the singleton instance since it's possible to add this script to multiple objects
			if ( _instance == this )
			{
				_instance = null;
			}
		}


		protected virtual void OnAwake()
		{
		}


		private void OnApplicationQuit()
		{
			_executing = false;
        }


		private bool CheckInstance()
		{
			var instanceOk = false;
			if ( _instance == null )
			{
				_instance = GetComponent<T>();

				if ( PersistsOnSceneChange )
				{
					DontDestroyOnLoad( this );
				}

				instanceOk = true;
			}

			DisableAllOtherInstances();
			return instanceOk;
		}
	}

}
