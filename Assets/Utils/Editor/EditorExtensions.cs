﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class EditorExtensions
{

	/// <summary>
	/// This shouldn't be really necessary, but as of UNity 5.4.2 property.tooltip was returning an empty string even on members with a Tooltip attribute.
	/// </summary>
	public static string GetTooltip( this PropertyDrawer propertyDrawer, SerializedProperty property )
	{
		if ( !string.IsNullOrEmpty( property.tooltip ) )
		{
			return property.tooltip;
		}

		var attributes = propertyDrawer.fieldInfo.GetCustomAttributes( typeof( TooltipAttribute ), true );
		for ( var index = 0; index < attributes.Length; ++index )
		{
			var attr = attributes[ index ];
			var tooltipAttribute = attr as TooltipAttribute;
			if ( tooltipAttribute != null )
			{
				return tooltipAttribute.tooltip;
			}
		}

		return null;
	}


	/// <summary>
	/// Handles clicks on the PropertyDrawer's label, "pinging" the target object on the project and hierarchy views.
	/// </summary>
	public static void PingTargetOnClick( this SerializedProperty property, Rect rect  )
	{
		var target = property.objectReferenceValue;

		if ( target == null )
		{
			return;
		}

		var evt = Event.current;
		if ( evt.type == EventType.MouseDown )
		{
			rect.width = EditorGUIUtility.labelWidth;
			var mousePos = evt.mousePosition;
			if ( rect.Contains( mousePos ) )
			{
				EditorGUIUtility.PingObject( ( ( Component ) target ).gameObject );
				evt.Use();
			}
		}
	}


	// From https://github.com/lordofduct/spacepuppy-unity-framework
	public static void SetValue( this SerializedProperty prop, object value )
	{
		if ( prop == null )
		{
			throw new System.ArgumentNullException( "prop" );
		}

		switch ( prop.propertyType )
		{
			case SerializedPropertyType.Integer:
				prop.intValue = ( int ) value;
				break;
			case SerializedPropertyType.Boolean:
				prop.boolValue = ( bool ) value;
				break;
			case SerializedPropertyType.Float:
				prop.floatValue = ( float ) value;
				break;
			case SerializedPropertyType.String:
				prop.stringValue = ( string ) value;
				break;
			case SerializedPropertyType.Color:
				prop.colorValue = ( Color ) value;
				break;
			case SerializedPropertyType.ObjectReference:
				prop.objectReferenceValue = value as Object;
				break;
			case SerializedPropertyType.LayerMask:
				prop.intValue = ( value is LayerMask ) ? ( ( LayerMask ) value ).value : ( int ) value;
				break;
			case SerializedPropertyType.Vector2:
				prop.vector2Value = ( Vector2 ) value;
				break;
			case SerializedPropertyType.Vector3:
				prop.vector3Value = ( Vector3 ) value;
				break;
			case SerializedPropertyType.Vector4:
				prop.vector4Value = ( Vector4 ) value;
				break;
			case SerializedPropertyType.Rect:
				prop.rectValue = ( Rect ) value;
				break;
			case SerializedPropertyType.ArraySize:
				prop.arraySize = ( int ) value;
				break;
			case SerializedPropertyType.Character:
				prop.intValue = ( int ) value;
				break;
			case SerializedPropertyType.AnimationCurve:
				prop.animationCurveValue = value as AnimationCurve;
				break;
			case SerializedPropertyType.Bounds:
				prop.boundsValue = ( Bounds ) value;
				break;

			default:
				throw new System.InvalidOperationException( string.Format( "Can not handle {0} type.", prop.propertyType ) );
		}
	}


	public static object GetPropertyValue< T >( this SerializedProperty prop )
	{
		return ( T ) GetPropertyValue( prop );
	}


	// From https://github.com/lordofduct/spacepuppy-unity-framework
	public static object GetPropertyValue( this SerializedProperty prop )
	{
		if ( prop == null )
		{
			throw new System.ArgumentNullException( "prop" );
		}

		switch ( prop.propertyType )
		{
			case SerializedPropertyType.Integer:
				return prop.intValue;
			case SerializedPropertyType.Boolean:
				return prop.boolValue;
			case SerializedPropertyType.Float:
				return prop.floatValue;
			case SerializedPropertyType.String:
				return prop.stringValue;
			case SerializedPropertyType.Color:
				return prop.colorValue;
			case SerializedPropertyType.ObjectReference:
				return prop.objectReferenceValue;
			case SerializedPropertyType.LayerMask:
				return ( LayerMask ) prop.intValue;
			case SerializedPropertyType.Enum:
				return prop.enumValueIndex;
			case SerializedPropertyType.Vector2:
				return prop.vector2Value;
			case SerializedPropertyType.Vector3:
				return prop.vector3Value;
			case SerializedPropertyType.Vector4:
				return prop.vector4Value;
			case SerializedPropertyType.Rect:
				return prop.rectValue;
			case SerializedPropertyType.ArraySize:
				return prop.arraySize;
			case SerializedPropertyType.Character:
				return ( char ) prop.intValue;
			case SerializedPropertyType.AnimationCurve:
				return prop.animationCurveValue;
			case SerializedPropertyType.Bounds:
				return prop.boundsValue;
			case SerializedPropertyType.Gradient:
				throw new System.InvalidOperationException( "Can not handle Gradient types." );
			default:
				throw new System.InvalidOperationException( "Can not handle property type: " + prop.propertyType );
		}
	}

}
