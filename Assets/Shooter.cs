﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{

    [SerializeField] GameObject BulletPrefab;
    [SerializeField] float m_projectileSpeed;
    [SerializeField] float m_bulletInitXPositionOffset;
    [SerializeField] float m_shootDelay;


    void Start()
    {
        StartCoroutine(ContinuousShoot());
    }

    private IEnumerator ContinuousShoot()
    {
        while (true)
        {
            yield return Shoot();
        }
    }

    private IEnumerator Shoot()
    {
        GameObject bullet = Instantiate(BulletPrefab);
        Rigidbody2D rigidbody = bullet.GetComponent<Rigidbody2D>();
        CapsuleCollider2D collider = bullet.GetComponent<CapsuleCollider2D>();
        rigidbody.transform.position = new Vector3(transform.position.x + -m_bulletInitXPositionOffset, transform.position.y + m_bulletInitXPositionOffset / 2f, transform.position.z);
        bullet.transform.Rotate(new Vector3(0f, 0f, 180f));
        rigidbody.velocity = new Vector2(-m_projectileSpeed, 0f);

        GameEvents.ShootCat();

        yield return new WaitForSeconds(m_shootDelay);
    }
}
