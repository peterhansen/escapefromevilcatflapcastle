﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    [SerializeField] float m_moveDistance = 2f;
    [SerializeField] float m_moveSpeed = 4f;
    [SerializeField] bool m_moveRight = true;

    [SerializeField] Rigidbody2D m_rigidBody;

    private float m_startX;
    private bool m_currMovingRight;
    private float m_deltaX;
    private float m_previousX;

    private void OnValidate()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        if (m_rigidBody == null)
        {
            m_rigidBody = gameObject.AddComponent<Rigidbody2D>();
            m_rigidBody.bodyType = RigidbodyType2D.Kinematic;
            m_rigidBody.freezeRotation = true;
        }
    }

    void Start () {
        m_startX = transform.position.x;
        m_deltaX = 0;
    }

    public float GetXVelocity()
    {
        return m_rigidBody.velocity.x;
    }

    public float GetDeltaX()
    {
        float reductionFactor = 0.98f;
        return m_deltaX * reductionFactor; // works better like this, don't ask.
    }
	
	void FixedUpdate () {
		if (m_moveRight)
        {
            if (m_currMovingRight)
            {
                if (transform.position.x - m_startX >= m_moveDistance)
                {
                    m_currMovingRight = false;
                    m_rigidBody.velocity = new Vector2(-m_moveSpeed, 0f);
                }
            }
            else
            {
                if (transform.position.x <= m_startX)
                {
                    m_currMovingRight = true;
                    m_rigidBody.velocity = new Vector2(m_moveSpeed, 0f);
                }
            }
        }
        else
        {

        }
        m_deltaX = transform.position.x - m_previousX;
        m_previousX = transform.position.x;
	}
}
