﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;


public class CameraController : MonoBehaviour
{

    [SerializeField] PlayerController m_player;

    [SerializeField] float m_speed;

    [SerializeField] Rigidbody2D m_rigidbody;

    [SerializeField] float m_yOffset;

	[SerializeField] private AnimationCurve m_playerHitShake;

	[SerializeField] private AnimationCurve m_enemyHitShake;


	private void Start()
	{
		GameEvents.CharacterHealthChanged += OnCharacterHealthChanged;
	}


	private void OnDestroy()
	{
		GameEvents.CharacterHealthChanged -= OnCharacterHealthChanged;
	}


	private void OnValidate()
    {
		if ( !m_rigidbody )
		{
			m_rigidbody = GetComponent<Rigidbody2D>();
		}
    }


    private void Update()
    {
        if (m_player != null && m_rigidbody != null)
        {
            m_rigidbody.velocity = m_speed * new Vector3(m_player.transform.position.x - transform.position.x, (m_player.transform.position.y + m_yOffset) - transform.position.y, 0f);
        }
        else if (m_player == null)
        {
            m_player = FindObjectOfType<PlayerController>();
        }

		if ( Input.GetKeyDown( KeyCode.X ) )
		{
			StartCoroutine( ShakeScreen( m_playerHitShake ) );
		}
    }


	private IEnumerator ShakeScreen( AnimationCurve shakeCurve )
	{
		var totalTime = shakeCurve.keys.Last().time;

		var accTime = 0.0f;
		while ( accTime < totalTime )
		{
			accTime += Time.deltaTime;
			var limit = shakeCurve.Evaluate( accTime );
			var offsetX = Random.Range( -limit, limit );
			var offsetY = Random.Range( -limit, limit );

			transform.position += new Vector3( offsetX, offsetY, 0.0f );

			yield return null;
		}
	}


	private void OnCharacterHealthChanged( GameCharacter obj )
	{
		var isHero = obj is Hero;
		StartCoroutine( ShakeScreen( isHero ? m_playerHitShake : m_enemyHitShake ) );
	}

}
