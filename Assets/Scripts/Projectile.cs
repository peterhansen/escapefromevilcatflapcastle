﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Projectile : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Projectile collided");
        int layerMask = collision.gameObject.layer;
        if (layerMask == GameLayer.GroundMask || layerMask == GameLayer.EnvironmentMask)
        {
            Debug.Log("Destroying arrow");
            Destroy(gameObject);
        }
        else if (layerMask == GameLayer.EnemyMask)
        {
            Destroy(gameObject);
            GameEvents.ProjectileHitObject(collision.gameObject);
        }
    }
}
