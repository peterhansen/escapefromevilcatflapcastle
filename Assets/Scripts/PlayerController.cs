﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : Hero
{

    [SerializeField] float m_horizontalAcc = 2f;
    [SerializeField] float m_horizontalTopSpeed = 7f;
    [SerializeField] float m_verticalSpeed = 15f;
    [SerializeField] float m_bounceForce = 150f;

    [SerializeField] bool m_canDoubleJump = false;
    [SerializeField] bool m_isOnFlyingCat = false;
    [SerializeField] float m_flyingCatFlapStrength = 7f;

    [SerializeField] GameObject BulletPrefab;

    [SerializeField] float m_shootDelay;
    [SerializeField] float m_projectileSpeed;
    [SerializeField] float m_bulletInitXPositionOffset = 0.3f;

    [SerializeField] float m_floorCollisionLimit = 15f;

    private bool m_enabled = true;
    private bool m_hasDoubleJumped = false;
    private bool m_hasFloorContact = false;

    private float m_timeOfLastShoot;

	private bool m_facingRight = true;

    RaycastHit2D[] m_floorcheck = new RaycastHit2D[10];

    public float HorizontalSpeed
	{
		get
		{
			return m_rigidbody.velocity.x;
		}
	}

	public bool Enabled
	{
		get { return m_enabled && HeroState != State.Dead; }
		set { m_enabled = value; }
	}


    protected override void Awake()
    {
		base.Awake();

        GameEvents.GameStateChanged += GameStateHasChanged;
    }


	protected override void OnDestroy()
    {
		base.OnDestroy();

        GameEvents.GameStateChanged -= GameStateHasChanged;
    }


    private bool FacingRight
	{
		get { return m_facingRight; }
		set
		{
			m_facingRight = value;
			var localScale = transform.localScale;
			localScale.x = value ? Mathf.Abs( localScale.x ) : -Mathf.Abs( localScale.x );
			transform.localScale = localScale;
		}
	}


    private void Start()
    {
        m_timeOfLastShoot = Time.time;
    }


    private void OnValidate()
    {
		if ( !m_rigidbody )
		{
			m_rigidbody = GetComponent<Rigidbody2D>();
		}
		if ( !m_collider )
		{
			m_collider = GetComponent<BoxCollider2D>();
		}
    }


	void FixedUpdate()
    {
        if ( !Enabled )
        {
            return;
        }

        m_hasFloorContact = false;
        bool adjustedByPlatform = false;
        if (m_rigidbody.velocity.y <= 0)
        {
            int count = m_collider.Raycast(Vector2.down, m_floorcheck, m_floorCollisionLimit);
            for (int i = 0; i < count; i++)
            {
                RaycastHit2D hit = m_floorcheck[i];
                int layer = hit.collider.gameObject.layer;
                if (layer == GameLayer.GroundMask ||
                    layer == GameLayer.EnvironmentMask ||
                    layer == GameLayer.EnemyMask)
                {
                    m_hasFloorContact = true;
                    m_hasDoubleJumped = false;

                    MovingPlatform platform = hit.collider.transform.parent.gameObject.GetComponent<MovingPlatform>();
                    if (platform != null && !adjustedByPlatform)
                    {
                        adjustedByPlatform = true;
                        m_rigidbody.position = new Vector2(m_rigidbody.position.x + platform.GetDeltaX(), m_rigidbody.position.y);
                    }
                }
            }
        }

		var walking = false;
        if (Input.GetKey(KeyCode.A) || Input.GetKey( KeyCode.LeftArrow ) )
        {
            m_rigidbody.velocity = new Vector2(Mathf.Max(m_rigidbody.velocity.x - m_horizontalAcc, -m_horizontalTopSpeed), m_rigidbody.velocity.y);
			FacingRight = false;
			walking = true;
		}
        else if (Input.GetKey(KeyCode.D) || Input.GetKey( KeyCode.RightArrow ) )
        {
            m_rigidbody.velocity = new Vector2(Mathf.Min(m_rigidbody.velocity.x + m_horizontalAcc, m_horizontalTopSpeed), m_rigidbody.velocity.y);
			FacingRight = true;
			walking = true;
		}
        else
        {
            m_rigidbody.velocity = new Vector2(Mathf.Max(m_rigidbody.velocity.x - m_horizontalAcc, 0f), m_rigidbody.velocity.y);
        }
		m_animator.SetBool( "Walking", walking );

        if ( ( Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown( KeyCode.UpArrow ) ) && (m_hasFloorContact || (m_canDoubleJump && !m_hasDoubleJumped) || m_isOnFlyingCat))
        {
            GameEvents.HeroJumps();


            Debug.Log("Jumping");
            if (!m_hasFloorContact)
            {
                m_hasDoubleJumped = true;
            }
            m_rigidbody.velocity = new Vector2(m_rigidbody.velocity.x, m_isOnFlyingCat ? m_flyingCatFlapStrength : m_verticalSpeed);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (m_timeOfLastShoot + m_shootDelay < Time.time)
            {
                Shoot();
            }
        }

		m_animator.SetBool( "Jumping", !m_hasFloorContact );
    }


    private void Shoot()
    {
		if ( Ammo > 0 )
		{
			GameObject bullet = Instantiate(BulletPrefab);
			Rigidbody2D rigidbody = bullet.GetComponent<Rigidbody2D>();
			CapsuleCollider2D collider = bullet.GetComponent<CapsuleCollider2D>();
			rigidbody.transform.position = new Vector3(transform.position.x + (m_facingRight ? m_bulletInitXPositionOffset : -m_bulletInitXPositionOffset), transform.position.y + m_bulletInitXPositionOffset/2f, transform.position.z);
			if (!FacingRight )
			{
				bullet.transform.Rotate(new Vector3(0f,0f,180f));
			}
			rigidbody.velocity = new Vector2( FacingRight ? m_projectileSpeed : -m_projectileSpeed, 0f);

            GameEvents.ShootCat();
            m_animator.SetTrigger("Shoot");

            --Ammo;
		}
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == GameLayer.EnvironmentMask)
        {   
            if (collision.contacts[0].normal.y < 0.5f)
            {
                Debug.Log("Bouncing");
                m_rigidbody.AddForce(new Vector2(collision.relativeVelocity.x > 0 ? m_bounceForce : -m_bounceForce, 0), ForceMode2D.Force);
            }
        }
    }


	private void GameStateHasChanged( GameState state )
	{
		switch ( state )
		{
			case GameState.Disabled:
			case GameState.NotInitialized:
			case GameState.Completed:
			case GameState.Splash:
				m_enabled = false;
				break;

			default:
				m_enabled = true;
				break;
		}
	}

}
