﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameCharacter : MonoBehaviour
{

	public const int MaxHealth = 5;

	[SerializeField] private int m_health = MaxHealth;

	[SerializeField] private float m_speed = 3.0f;

	
	public virtual int Health
	{
		get { return m_health; }
		set
		{
			var clampedValue = Mathf.Clamp( value, 0, 5 );
			if ( m_health != clampedValue )
			{
				m_health = clampedValue;

				GameEvents.CharacterHealthChanged( this );

				if ( m_health == 0 )
				{
					GameEvents.OnCharacterDied( this );
				}
			}
		}
	}

	public float Speed
	{
		get { return m_speed; }
		set { m_speed = value; }
	}

}
