﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    [SerializeField] private CanvasGroup m_canvasGroup;
    [SerializeField] private Text m_dialogueText;
    [SerializeField] private CanvasGroup m_cinemaCanvas;
    [SerializeField] private Animator m_talking;

    private void Awake()
    {
        GameEvents.DialogueHasStarted += DialogueHasStarted;

        m_canvasGroup.alpha = 0.0f;
        m_cinemaCanvas.alpha = 0.0f;
    }

    private void Update()
    {
       if (Input.GetKeyDown(KeyCode.P))
        {
            GameEvents.DialogueHasStarted(new[] { "Use arrows to move, up to shoot", "Use space to jump!", "You'll never escape my castle!!" });
        }

    }

    private void DialogueHasStarted(string[] dialogue)
    {
        StartCoroutine(ShowDialogue(dialogue));
    }

    private void OnDestroy()
    {
        GameEvents.DialogueHasStarted -= DialogueHasStarted;
    }

    IEnumerator ShowDialogue (string[] dialogue)
    { 
        m_cinemaCanvas.alpha = 1.0f;
        GameEvents.GameStateChanged(GameState.Disabled);
        yield return new WaitForSeconds(0.5f);

        foreach (string i in dialogue)
        {
            m_talking.SetBool("Talking", true);
            // bring up dialogue
            m_canvasGroup.alpha = 1.0f;
            m_dialogueText.text = i;

            // wait for it to be finished or key pressed then continue
            yield return new WaitForSeconds(3.0f);
        }

        m_talking.SetBool("Talking", false);

        m_canvasGroup.alpha = 0.0f;
        GameEvents.GameStateChanged(GameState.Playing);
        m_cinemaCanvas.alpha = 0.0f;

    }
}
