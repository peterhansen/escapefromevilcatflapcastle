﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameStateManager : Singleton< GameStateManager >
{

	[SerializeField] private Transform m_playerSpawnTransfrom;

	[Header( "Prefabs" )]
	[SerializeField, PrefabPicker] private Hero m_heroPrefab;

	private GameState m_state = GameState.NotInitialized;

	private Transform m_lastCheckpointTransform;

	private Hero m_hero;

	public Hero Hero
	{
		get { return m_hero; }
	}


	protected override void OnAwake()
	{
		base.OnAwake();
		m_hero = Instantiate( m_heroPrefab );
		m_hero.transform.SetParent( m_playerSpawnTransfrom, false );

		m_lastCheckpointTransform = m_playerSpawnTransfrom;

		GameEvents.OnCharacterDied += OnCharacterDied;
		GameEvents.OnPickup += OnPickup;
	}


	private void Start()
	{
		SetState( GameState.Splash );
	}


	private void Update()
	{
		if ( Input.GetKeyDown( KeyCode.Escape ) )
		{
			Application.Quit();
		}
	}


	protected override void OnDestroy()
	{
		base.OnDestroy();

		GameEvents.OnCharacterDied -= OnCharacterDied;
		GameEvents.OnPickup -= OnPickup;
	}


	private void SetState( GameState state )
	{
		if ( state == m_state )
		{
			return;
		}

		m_state = state;

		switch ( state )
		{
			case GameState.Splash:
				StartCoroutine( SplashCoroutine() );
				break;
		}

		GameEvents.GameStateChanged( state );
	}


	private IEnumerator SplashCoroutine()
	{
		yield return new WaitForSeconds( 2.0f );

		while ( m_state == GameState.Splash )
		{
			if ( Input.anyKeyDown )
			{
				SetState( GameState.Playing );
			}

			yield return null;
		}
	}


	private void OnPickup( Pickup pickup )
	{
		switch ( pickup.Type )
		{
			case PickupType.CheckPoint:
				m_lastCheckpointTransform = pickup.transform;
				Debug.LogFormat( "Respawn position updated: {0}", pickup.transform.position );
				break;

			case PickupType.Exit:
				if ( m_hero.HeroState != Hero.State.Dead )
				{
					SetState( GameState.Completed );
				}
				break;
		}
	}


	private void OnCharacterDied( GameCharacter gameCharacter )
	{
		if ( gameCharacter != m_hero )
		{
			return;
		}

		var hero = ( Hero ) gameCharacter;
		if ( hero.Lives == 0 )
		{
			SetState( GameState.GameOver );
		}
		else
		{
			StartCoroutine( RespawnCoroutine() );
		}
	}


	private IEnumerator RespawnCoroutine()
	{
		yield return new WaitForSeconds( 1.5f );

		m_hero.transform.position = m_lastCheckpointTransform.position;
		m_hero.HeroState = Hero.State.Invincible;
	}
}
