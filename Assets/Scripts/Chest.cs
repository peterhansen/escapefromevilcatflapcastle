﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Chest : Pickup
{

	protected override void OnPickedUp()
	{
		base.OnPickedUp();

		Animator.SetBool( "Open", true );
		Collider.enabled = false;
	}

}
