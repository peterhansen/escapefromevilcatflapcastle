﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroLivesView : MonoBehaviour
{

	[SerializeField, ChildComponent] private Text m_livesLabel;


	private void Start()
	{
		var lives = GameStateManager.Instance.Hero.Lives;
		SetLives( lives );
		
		GameEvents.HeroLivesChanged += OnHeroLivesChanged;
	}


	private void OnDestroy()
	{
		GameEvents.HeroLivesChanged -= OnHeroLivesChanged;
	}


	private void OnHeroLivesChanged( Hero hero )
	{
		var lives = hero.Lives;
		SetLives( lives );
	}


	private void SetLives( int lives )
	{
		m_livesLabel.text = lives.ToString();
	}

}
