﻿using PitecoHansen;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseScreenView : MonoBehaviour
{

	[SerializeField] private GameState m_gameState;

	[SerializeField, AutoAdd] private Animator m_animator;

	[SerializeField, AutoAdd] private CanvasGroup m_canvasGroup;

	public Animator Animator { get { return m_animator; } }


	private void Awake()
	{
		GameEvents.GameStateChanged += OnGameStateChanged;
	}


	private void OnDestroy()
	{
		GameEvents.GameStateChanged -= OnGameStateChanged;
	}


	private void OnGameStateChanged( GameState state )
	{
        if (state == GameState.Completed)
        {
            m_animator.SetTrigger("End");
        }
		m_animator.SetBool( "Visible", state == m_gameState );
	}

}
