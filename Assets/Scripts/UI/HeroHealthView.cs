﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HeroHealthView : MonoBehaviour
{

	[SerializeField] private GameObject m_healthIconPrefab;

	private readonly List<Animator> m_healthIconAnimators = new List<Animator>();


	private void Start()
	{
		transform.DestroyAllChildren();

		for ( var i = 0; i < Hero.MaxHealth; ++i )
		{
			var icon = Instantiate( m_healthIconPrefab );
			icon.transform.SetParent( transform, false );
			m_healthIconAnimators.Add( icon.GetComponent< Animator >() );
		}

		var health = GameStateManager.Instance.Hero.Health;
		SetHealth( health );

		GameEvents.CharacterHealthChanged += OnCharacterHealthChanged;
	}


	private void OnDestroy()
	{
		GameEvents.CharacterHealthChanged -= OnCharacterHealthChanged;
	}


	private void OnCharacterHealthChanged( GameCharacter character )
	{
		if ( character.gameObject.layer != GameLayer.HeroMask )
		{
			return;
		}

		SetHealth( character.Health );
	}


	private void SetHealth( int health )
	{
		for ( var i = 0; i < m_healthIconAnimators.Count; ++i )
		{
			m_healthIconAnimators[ i ].SetBool( "On", i < health );
		}
	}

}
