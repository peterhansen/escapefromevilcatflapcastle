﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SplashView : BaseScreenView
{
    [SerializeField] private AudioClip m_meow;

    public void Meow()
    {
		if ( m_meow )
		{
			AudioSource.PlayClipAtPoint(m_meow, Vector3.zero);
		}
    }
}
