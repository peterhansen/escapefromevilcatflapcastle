﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroAmmoView : MonoBehaviour
{
	// hi pete
	private const int MAX_CATS = 69;
	private int m_numberOfCatsYouHaveInYourBackpackButTheyreProbablyDeadNotAlive = MAX_CATS;

	[SerializeField, ChildComponent] private Text m_ammoCountLabel;


	private void Start()
	{
		GameEvents.HeroAmmoChanged += OnHeroAmmoChanged;
		SetAmmo( GameStateManager.Instance.Hero.Ammo );
	}


	private void OnDestroy()
	{
		GameEvents.HeroAmmoChanged -= OnHeroAmmoChanged;
	}


	private void OnHeroAmmoChanged( Hero hero )
	{
		SetAmmo( hero.Ammo );
	}


	private void SetAmmo( int ammo )
	{
		m_ammoCountLabel.text = "x" + ammo;
	}

}
