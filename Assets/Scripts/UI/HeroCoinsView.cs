﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HeroCoinsView : MonoBehaviour
{

	[SerializeField, ChildComponent] private Text m_coinsLabel;


	private void Start()
	{
		var coins = GameStateManager.Instance.Hero.Coins;
		SetCoins( coins );
		
		GameEvents.HeroCoinsChanged += OnHeroCoinsChanged;
	}


	private void OnDestroy()
	{
		GameEvents.HeroCoinsChanged -= OnHeroCoinsChanged;
	}


	private void OnHeroCoinsChanged( Hero hero )
	{
		SetCoins( hero.Coins );
	}


	private void SetCoins( int coins )
	{
		m_coinsLabel.text = coins.ToString( "D3" );
	}

}
