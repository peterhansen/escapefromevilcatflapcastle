﻿using PitecoHansen;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pickup : MonoBehaviour
{

	[SerializeField, AutoAdd] private BoxCollider2D m_collider;

	[SerializeField, AutoAdd] private Animator m_animator;

	[SerializeField] private PickupType m_type;


	public PickupType Type
	{
		get { return m_type; }
	}

	public Animator Animator
	{
		get { return m_animator; }
	}

	public BoxCollider2D Collider
	{
		get
		{
			return m_collider;
		}
	}

	private void OnCollisionEnter2D( Collision2D collision )
	{
		HandleCollision( collision.collider );
	}


	private void OnTriggerStay2D( Collider2D collider )
	{
		HandleCollision( collider );
	}


	private void OnCollisionStay2D( Collision2D collision )
	{
		HandleCollision( collision.collider );
	}


	private void OnTriggerEnter2D( Collider2D collider )
	{
		HandleCollision( collider );
	}


	private void HandleCollision( Collider2D collider )
	{
		if ( collider.gameObject.layer == GameLayer.HeroMask )
		{
			OnPickedUp();
		}
	}


	protected virtual void OnPickedUp()
	{
		GameEvents.OnPickup( this );
	}

}
