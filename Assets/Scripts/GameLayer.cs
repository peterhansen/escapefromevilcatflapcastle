﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameLayer
{

	public static readonly string Hero = "Hero";

	public static readonly int HeroMask = LayerMask.NameToLayer( Hero );

	public static readonly string Enemy = "Enemy";

	public static readonly int EnemyMask = LayerMask.NameToLayer( Enemy );

	public static readonly string Pickup = "Pickup";

	public static readonly int PickupMask = LayerMask.NameToLayer( Pickup );

	public static readonly string Environment = "Environment";

	public static readonly int EnvironmentMask = LayerMask.NameToLayer( Environment );

    public static readonly string Ground = "Ground";

    public static readonly int GroundMask = LayerMask.NameToLayer(Ground);

}
