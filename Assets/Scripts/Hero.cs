﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Hero : GameCharacter
{

	public enum State
	{
		Alive,
		Invincible,
		Dead,
	}

	public const int MaxLives = 3;

	public const int MaxAmmo = 7;

	[SerializeField] private float m_invincibilityTime = 3.0f;

	[SerializeField] private float m_ammoRefillTime = 1.0f;

	[SerializeField] private float m_burnVerticalSpeed = 100.0f;

	[SerializeField, AutoAdd] protected Animator m_animator;

	[SerializeField, AutoAdd] protected Collider2D m_collider;

	[SerializeField, AutoAdd ] protected Rigidbody2D m_rigidbody;

	private int m_lives = MaxLives;

	private int m_ammo = MaxAmmo;

	private int m_coins;

	private State m_state;


	public override int Health
	{
		get { return base.Health; }
		set
		{
			var previousHealth = Health;
			base.Health = value;

			if ( previousHealth == Health )
			{
				return;
			}

			if ( Health <= 0 )
			{
				Health = MaxHealth;
				HeroState = State.Dead;
				--Lives;
			}
			else
			{
				HeroState = State.Invincible;
			}
		}
	}

	public State HeroState
	{
		get { return m_state; }
		set
		{
			if ( m_state == value )
			{
				return;
			}

			Debug.LogFormat( gameObject, "<color=yellow>Hero state: {0} -> {1}</color>", m_state, value );
			m_state = value;

			switch ( value )
			{
				case State.Alive:
					m_animator.SetBool( "Dead", false );
					break;

				case State.Invincible:
					m_animator.SetBool( "Dead", false );
					StartCoroutine( InvincibilityCoroutine() );
					break;
				case State.Dead:
					m_animator.SetBool( "Dead", true );
					break;
			}
		}
	}


	public int Lives
	{
		get { return m_lives; }
		set
		{
			var clampedValue = Mathf.Clamp( value, 0, MaxLives );
			if ( clampedValue != m_lives )
			{
				m_lives = clampedValue;
				GameEvents.HeroLivesChanged( this );
			}
		}
	}

	public int Ammo
	{
		get { return m_ammo; }
		set
		{
			var clampedValue = Mathf.Clamp( value, 0, MaxAmmo );
			if ( clampedValue != m_ammo )
			{
				m_ammo = value;
				GameEvents.HeroAmmoChanged( this );
			}
		}
	}

	public int Coins
	{
		get { return m_coins; }
		set
		{
			var clampedValue = Mathf.Clamp( value, 0, 999 );
			if ( clampedValue != m_coins )
			{
				m_coins = value;
				GameEvents.HeroCoinsChanged( this );
			}
		}
	}


	protected virtual void Awake()
	{
		GameEvents.OnPickup += OnPickup;
		StartCoroutine( RefillAmmoCoroutine() );
	}


	protected virtual void OnDestroy()
	{
		GameEvents.OnPickup -= OnPickup;
	}


	private void OnCollisionEnter2D( Collision2D collision )
	{
		if ( collision.gameObject.layer != GameLayer.EnemyMask )
		{
			return;
		}

		if ( m_state == State.Alive )
		{
			--Health;
		}
	}


	private void OnTriggerEnter2D( Collider2D collision )
	{
		if ( collision.gameObject.layer != GameLayer.EnemyMask )
		{
			return;
		}

		if ( m_state == State.Alive )
		{
			--Health;
		}
	}


	private void OnPickup( Pickup pickup )
	{
		switch ( pickup.Type )
		{
			case PickupType.Ammo:
				++Ammo;
				break;
			case PickupType.Coin:
				++Coins;
				break;
			case PickupType.Cat:
				++Health;
				break;
			case PickupType.Lava:
				if ( m_state == State.Alive )
				{
					--Health;
					m_animator.SetTrigger( "Burn" );
					m_rigidbody.velocity += new Vector2( m_rigidbody.velocity.x, m_burnVerticalSpeed );
				}
				break;
			default:
				Debug.LogErrorFormat( "Unhandled pickup type: {0}", pickup.Type );
				break;
		}
	}


	private IEnumerator InvincibilityCoroutine()
	{
		m_animator.SetBool( "Invincible", true );
		yield return new WaitForSeconds( m_invincibilityTime );
		m_animator.SetBool( "Invincible", false );
		HeroState = State.Alive;
	}


	private IEnumerator RefillAmmoCoroutine()
	{
		while ( true )
		{
			if ( HeroState != State.Dead )
			{
				if ( Ammo < MaxAmmo )
				{
					yield return new WaitForSeconds( m_ammoRefillTime );
					++Ammo;
				}
			}

			yield return null;
		}
	}

}
