﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaddieMover : MonoBehaviour
{

    [SerializeField] Transform m_walkTo;
    [SerializeField] float m_speed = 3f;
    [SerializeField] float m_atWaypointLimit = 1f;
    [SerializeField] bool m_dontChangeLookDirection = false;

    private int m_currWaypointIndex = 0;

    private Vector3 m_updateVector;
    private Vector3 m_currentWaypoint;

    private List<Vector3> m_waypoints;

    private Vector3 m_limit = new Vector3(0.1f, 0.1f, 0f);

    private float m_xScale;

    private RaycastHit2D[] m_floorCheck;

    private void Start()
    {
        m_xScale = transform.localScale.x;
        m_waypoints = new List<Vector3>(2);
        m_waypoints.Add(transform.position);
        m_waypoints.Add(m_walkTo.position);
        SetNextWaypoint();
    }

    private void SetNextWaypoint()
    {
        m_currWaypointIndex++;
        if (m_currWaypointIndex >= m_waypoints.Count)
        {
            m_currWaypointIndex = 0;
        }
        m_currentWaypoint = new Vector3(m_waypoints[m_currWaypointIndex].x, m_waypoints[m_currWaypointIndex].y, 0f);
    }

    void Update()
    {
        m_updateVector.x = m_currentWaypoint.x - transform.position.x;
        if (!m_dontChangeLookDirection)
        {
            transform.localScale = new Vector3(m_updateVector.x > 0 ? m_xScale : -m_xScale, transform.localScale.y, transform.localScale.z);
        }
        m_updateVector.y = m_currentWaypoint.y - transform.position.y;
        m_updateVector.Normalize();

        transform.position += (m_speed / 100f) * m_updateVector; // dividing by 100 puts speed in a more normal range

        if (Mathf.Abs(m_currentWaypoint.x - transform.position.x) < m_atWaypointLimit && Mathf.Abs(m_currentWaypoint.y - transform.position.y) < m_atWaypointLimit)
        {
            SetNextWaypoint();
        }

    }
}
