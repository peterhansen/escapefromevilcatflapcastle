﻿using System;
using UnityEngine;

public static class GameEvents
{

	public static Action<GameCharacter> OnCharacterDied = ( character ) => { };

	public static Action<Pickup> OnPickup = ( Pickup ) => { };

	public static Action<Hero> HeroLivesChanged = ( hero ) => { };

	public static Action<GameCharacter> CharacterHealthChanged = ( character ) => { };

	public static Action<Hero> HeroAmmoChanged = ( hero ) => { };

	public static Action<Hero> HeroCoinsChanged = ( hero ) => { };

	public static Action<GameState> GameStateChanged = ( gameState ) => { };
	public static Action<string[]> DialogueHasStarted = ( dialogueString ) => { };
	public static Action<GameObject> ProjectileHitObject = ( go ) => { };

    public static Action CatObtained = () => { };
    public static Action ShootCat = () => { };
    public static Action SkelentonDies = () => { };
    public static Action HeroJumps = () => { };
}
