﻿using PitecoHansen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameAudioManager : MonoBehaviour
{

	private enum EventNames
	{
        BackgroundMusic,
		ProjectileHitObject,
		Pickup,
		CharacterDied,
		HeroLivesChanged,
		HeroAmmoChanged,
		CharacterHealthChanged,
        CatObtained,
        CatShoot,
        SkelentonDies,
        HeroJumps,
    }

	[ Serializable ]
	private struct AudioInfo
	{
		public EventNames EventName;
		public AudioClip[] Audio;
		public bool Loop;

	}

	[SerializeField, Range( 5, 50 ), ReadOnly( ReadOnlyMode.DisableWhenPlaying )]
	private int m_poolSize = 20;

	[SerializeField] private AudioInfo[] m_audioInfo;

	private readonly List<AudioSource> m_audioPool = new List<AudioSource>();

	private int m_lastIndex;


	private void Awake()
	{
		m_audioPool.Capacity = m_poolSize;
		for ( var i = 0;i < m_poolSize; ++i )
		{
			var audioSourceGameObject = new GameObject( "AudioPool_" + i );
			audioSourceGameObject.transform.SetParent( transform, false );
			var audioSource = audioSourceGameObject.AddComponent<AudioSource>();
			m_audioPool.Add( audioSource );
		}

		GameEvents.CharacterHealthChanged += OnCharacterHealthChanged;
		GameEvents.HeroAmmoChanged += OnHeroAmmoChanged;
		GameEvents.HeroLivesChanged +=HeroLivesChanged;
		GameEvents.OnCharacterDied += OnCharacterDied;
		GameEvents.OnPickup += OnPickup;
		GameEvents.ProjectileHitObject += OnProjectileHitObject;
        GameEvents.CatObtained += CatObtained;
        GameEvents.ShootCat += CatShoot;
        GameEvents.SkelentonDies += SkeletonDies;
        GameEvents.HeroJumps += HeroJumps;

        BackgroundMusic();
	}


	private void OnDestroy()
	{
		
	}


	private void PlayAudio( EventNames eventName )
	{
		AudioInfo audioInfo = new AudioInfo();
		var found = false;
		for ( var i= 0; i < m_audioInfo.Length; ++i )
		{
			if ( m_audioInfo[ i ].EventName == eventName )
			{
				audioInfo = m_audioInfo[ i ];
				found = true;
				break;
			}
		}

		if ( !found )
		{
			return;
		}

		for ( var i = 0; i < m_poolSize; ++i )
		{
			var index = ( m_lastIndex + i ) % m_poolSize;
			var audio = m_audioPool[ i ];
			if ( audio.clip == null || !audio.isPlaying )
			{

                    audio.clip = audioInfo.Audio[UnityEngine.Random.Range(0, audioInfo.Audio.Length)];
                if (audio.clip != null)
                {
                    audio.loop = audioInfo.Loop;
                    audio.Play();
                    break;
                }
			}
		}
	}

    private void SkeletonDies()
    {
        PlayAudio(EventNames.SkelentonDies);
    }


    private void CatShoot()
    {
        PlayAudio(EventNames.CatShoot);
    }

    private void HeroJumps()
    {
        PlayAudio(EventNames.HeroJumps);
    }

    private void OnProjectileHitObject( GameObject obj )
	{
		PlayAudio( EventNames.ProjectileHitObject );
	}

	private void OnPickup( Pickup obj )
	{
		PlayAudio( EventNames.Pickup );
	}

    private void CatObtained()
    {
        Debug.Log("Playing MEOW");
        PlayAudio(EventNames.CatObtained);
    }

	private void OnCharacterDied( GameCharacter obj )
	{
		PlayAudio( EventNames.CharacterDied );
	}

	private void HeroLivesChanged( Hero obj )
	{
		PlayAudio( EventNames.HeroLivesChanged );
	}

	private void OnHeroAmmoChanged( Hero obj )
	{
		PlayAudio( EventNames.HeroAmmoChanged );
	}

	private void OnCharacterHealthChanged( GameCharacter obj )
	{
		PlayAudio( EventNames.CharacterHealthChanged );
	}

    private void BackgroundMusic()
    {
        PlayAudio(EventNames.BackgroundMusic);
    }

}
