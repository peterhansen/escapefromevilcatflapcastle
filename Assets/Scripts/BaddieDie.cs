﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaddieDie : MonoBehaviour {

    [SerializeField] GameObject m_explosion;

	private void Awake () {
        GameEvents.ProjectileHitObject += ObjectHitListener;
	}

    private void OnDestroy()
    {
        GameEvents.ProjectileHitObject -= ObjectHitListener;
    }

    private void ObjectHitListener(GameObject obj)
    {
        if (obj == gameObject)
        {
            Debug.Log("Object hit me : " + name);
            Die();
        }
    }

    private void Die()
    {
        GameEvents.SkelentonDies();
        Instantiate(m_explosion, new Vector3(transform.position.x, transform.position.y, 0f), Quaternion.Euler(0f,0f,0f));
        Destroy(gameObject);
    }
}
