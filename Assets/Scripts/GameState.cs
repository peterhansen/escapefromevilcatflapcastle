﻿public enum GameState
{
	NotInitialized,
	Splash,
	Playing,
	GameOver,
    Disabled,
	Completed,
}
