﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatPickup : MonoBehaviour
{
    [SerializeField] float m_fadeOutTime = 1f;

    [SerializeField] SpriteRenderer m_spriteRenderer;

    [SerializeField] BoxCollider2D m_boxCollider;

    private void OnValidate()
    {
        m_spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        m_boxCollider = gameObject.GetComponent<BoxCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == GameLayer.HeroMask)
        {
            StartCoroutine(Pickup());
        }
    }

    private IEnumerator Pickup()
    {
        GameEvents.CatObtained();
        m_boxCollider.enabled = false;
        yield return FadeOut();
        Destroy(gameObject);
    }

    IEnumerator FadeOut()
    {
        for (float t = 1f; t >= 0f; t -= Time.deltaTime / m_fadeOutTime)
        {
            Color newColor = new Color(1, 1, 1, t);
            m_spriteRenderer.material.color = newColor;
            yield return null;
        }
    }
}
