﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CheckPoint : Pickup
{

	protected override void OnPickedUp()
	{
		base.OnPickedUp();

		Animator.SetBool( "On", true );
		Collider.enabled = false;
	}

}
